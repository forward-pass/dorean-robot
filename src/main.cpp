#include <Arduino.h>
#include <vector>
#include "motor_control.h"
#include "user_input.h"

MotorControl J5;
MotorControl J6;

enum CommandMotor {
    CommandAll,
    CommandJ5,
    CommandJ6
};

CommandMotor motorSelect = CommandMotor::CommandAll;

void ProcessCommand(MotorControl &motor, String command, std::vector<String> parameters) {
    if (command == "v" || command == "verbose") {
        bool verbose = motor.ToggleVerbose();
        Serial.print("Toggle verbose logging: ");
        Serial.println(verbose);
    }
    else if (command == "s" || command == "speed") {
        float speedRPM = motor.GetMaxSpeedRPM();
        if (parameters.size() >= 1) {
            speedRPM = parameters[0].toFloat();
        }
        motor.SetMaxSpeedRPM(speedRPM);

        Serial.print("Set max speed: ");
        Serial.print(motor.GetMaxSpeedRPM());
        Serial.println("rpm");
    }
    else if (command == "p" || command == "+") {
        float newSpeedRPM = motor.GetMaxSpeedRPM() + 5.0f;
        motor.SetMaxSpeedRPM(newSpeedRPM);

        Serial.print("Set max speed: ");
        Serial.print(motor.GetMaxSpeedRPM());
        Serial.println("rpm");
    }
    else if (command == "m" || command == "-") {
        float newSpeedRPM = motor.GetMaxSpeedRPM() - 5.0f;
        motor.SetMaxSpeedRPM(newSpeedRPM);

        Serial.print("Set max speed: ");
        Serial.print(motor.GetMaxSpeedRPM());
        Serial.println("rpm");
    }
    else if (command == "r" || command == "reset") {
        motor.ResetStall();
        Serial.println("Reset stall");
    }
    else if (command == "t" || command == "test") {
        // Toggle off test mode.
        if (motor.ModeState == MotorControl::OperationMode::TestMode) {
            motor.StopTest();
        }
        else {
            float maxPos = 500;
            if (parameters.size() >= 1) {
                maxPos = parameters[0].toFloat();
            }
            Serial.println("Starting Test mode");
            motor.TestCycle(maxPos);
        }
    }
    else if (command == "c" || command == "cal" || command == "calibrate") {
        Serial.println("Starting Calibration mode");
        motor.CalibrateRangeOfMotion();
    }
    else if (command == "step") {
        if (parameters.size() < 1) {
            Serial.println("'step' called with no parameters. Provided number of steps to perform.");
        }
        else {
            int stepCount = 0;
            stepCount = parameters[0].toInt();
            motor.Step(stepCount);
        }
    }
    else if (command == "move") {
        if (parameters.size() < 1) {
            Serial.println("'move' called with no parameters. Provided a percentage of range of motion to perform (between 0 and 100).");
        }
        else {
            float movePercentage = 0;
            movePercentage = parameters[0].toFloat();
            motor.Move(movePercentage);
        }
    }
}

void setup() {
    Serial.begin(115200);
    Serial.println();

    Serial.println("press any key.");
    while (!Serial.available());

    // The SS pin shouldn't matter which motor's ss pin to use.
    // In this case I just used 5 as that was used in J5 and is the default.
    SPI.begin(18, 19, 23, 5);

    J5 = MotorControl(5);
    J6 = MotorControl(4);

    J5.CheckMotorConnection();
    J5.InitializeMotor();

    J6.CheckMotorConnection();
    J6.InitializeMotor();
}

void loop() {
    J5.ControlLoop();
    J6.ControlLoop();

    if (Serial.available() > 0) {
        String input = UserInput::GetInput();

        Serial.print(":");
        Serial.println(input);

        String command = "";
        std::vector<String> parameters;

        UserInput::ParseCommand(input, &command, &parameters);

        if (command == "h" || command == "help") {
            Serial.println("Motor Controller Commands:");
            Serial.println("'h'\t'help'\tPrints this message.");
            Serial.println();
            Serial.println("To select a specific motor, pass in a number for which motor to select.");
            Serial.println("e.g. '1' for J1 motor, '5' for J5 motor, or '0' for all motors");
            Serial.println();
            Serial.println("'v'\t'verbose'\tToggles verbose logging on serial of motor status.");
            Serial.println("'r'\t'reset'\t\tReset stall. If motor does not move but should be, it might be stalled.");
            Serial.println("'c'\t'cal'\t\tStart motor range of motion calibration sequence.");
            Serial.println("'t'\t'test'\t\tMoves motor back and forth. After calibration, it will move its full range of motion.");
            Serial.println("'s'\t'speed'\t\tSet the motor speed in rpm.");
            Serial.println("'p'\t'+'\t\tIncrement the speed by 5 rpm.");
            Serial.println("'m'\t'-'\t\tDecrement the speed by 5 rpm.");
            Serial.println("\t'step'\t\tMoves the motor by a number of steps.");
            Serial.println("\t'move'\t\tMoves the motor by a percentage of range of motion (0.0-100.0). Requires calibration first.");
        }
        else if (command == "0") {
            motorSelect = CommandMotor::CommandAll;
            Serial.println("Commanding all motors");
        }
        else if (command == "5") {
            motorSelect = CommandMotor::CommandJ5;
            Serial.println("Commanding J5");
        }
        else if (command == "6") {
            motorSelect = CommandMotor::CommandJ6;
            Serial.println("Commanding J6");
        }

        switch (motorSelect)
        {
        case CommandMotor::CommandAll:
            ProcessCommand(J5, command, parameters);
            ProcessCommand(J6, command, parameters);
            break;
        case CommandMotor::CommandJ5:
            ProcessCommand(J5, command, parameters);
            break;
        case CommandMotor::CommandJ6:
            ProcessCommand(J6, command, parameters);
            break;
        
        default:
            break;
        }
    }

    delay(10);
}