


## Setup Guide

* Get VSCode if you already haven't.
* In VScode extensions, get the PlatformIO extension. There are plenty of guides on how to use PlatformIO here: https://docs.platformio.org/en/latest/
* Here are a few notable tutorials:
    * https://docs.platformio.org/en/stable/tutorials/espressif32/arduino_debugging_unit_testing.html
    * https://piolabs.com/blog/insights/unit-testing-part-1.html

### Windows GCC
* If running on Windows, check gcc works using this guide: https://code.visualstudio.com/docs/cpp/config-mingw
* If gcc command does not work, it will tell you to install MSYS2 here: https://www.msys2.org/
* Follow the guide to run the MSYS2 installer and run the MSYS2 terminal.
* In the terminal, run the pacman command with default install options.
```
pacman -S --needed base-devel mingw-w64-x86_64-toolchain
```
* Then add the msys install path to you windows PATH variable. If default install path, it would be this:
```
C:\msys64\mingw64\bin
```
* Finally start up a new terminal like cmd and run:
```
gcc --version
```
* It should show that it works.