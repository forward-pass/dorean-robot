


#ifndef USER_INPUT_H
#define USER_INPUT_H

#include <Arduino.h>
#include <vector>

class UserInput {
public:
    static const char NEWLINE = (char)10;
    static const char CARAGE_RETURN = (char)13;
    static const char SPACE = (char)32;
    static const char BACKSPACE = (char)8;

    
    /// @brief Raw Serial input until newline character.
    /// @param verbose Prints useful info about the text like HEX values.
    /// @return The input in string format.
    static String GetRawInput(bool verbose=false);

    /// @brief Removes newlines and carriage returns and process backspace.
    /// @param verbose Prints useful info about the text and processes like HEX values.
    /// @param overrideInput Bypasses serial input for debugging purposes.
    /// @return The processed clean input in string format.
    static String GetInput(bool verbose=false, String overrideInput="");

    /// @brief Splits the string between delimiters and returns the first word
    /// as the command and the following words as an array of parameters.
    /// @param input The input string that needs to be split.
    /// @param command The first word in input, or the command word.
    /// @param parameters The subsequent words in input which can be an array of parameters.
    /// @param delimiter The separator character between words, default to " ".
    /// @param maxParameterCount A maximum expected number of parameters in input. Because arrays aren't dynamic.
    static void ParseCommand(String input, String* command, std::vector<String>* parameters,
        char delimiter=SPACE, size_t maxParameterCount=64);

private:

};

#endif // USER_INPUT_H