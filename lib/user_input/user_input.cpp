
#include "user_input.h"

String UserInput::GetRawInput(bool verbose)
{
    String input = Serial.readStringUntil(NEWLINE);

    if (verbose) {
        Serial.print("raw: ");
        Serial.println(input);
        Serial.print("len: ");
        Serial.println(input.length());

        Serial.print("hex: ");
        for (size_t i = 0; i < input.length(); i++) {
            Serial.print(input[i], HEX);
            Serial.print(" ");
        }
        Serial.println();
    }

    return input;
}

String UserInput::GetInput(bool verbose, String overrideInput)
{
    String input;
    if (overrideInput != "") {
        input = overrideInput;
        if (verbose) {
            Serial.print("raw: ");
            Serial.println(input);
            Serial.print("len: ");
            Serial.println(input.length());

            Serial.print("hex: ");
            for (size_t i = 0; i < input.length(); i++) {
                Serial.print(input[i], HEX);
                Serial.print(" ");
            }
            Serial.println();
        }
    }
    else {
        input = GetRawInput(verbose);
    }

    // Removes unnecessary endings
    while ( input.endsWith(String(NEWLINE))         || 
            input.endsWith(String(CARAGE_RETURN))   ||
            input.endsWith(String(SPACE))           ) {
        input.remove(input.length()-1, 1);
    }

    // Properly removes backspaces
    int indexFound = input.indexOf(BACKSPACE);
    while (indexFound != -1) {
        if (verbose) {
            Serial.print("backspace found at: ");
            Serial.println(indexFound);
        }
        // remove backspaces char
        input.remove(indexFound, 1);
        // remove mistake char
        if (indexFound != 0) {
            if (verbose) {
                Serial.print("char: ");
                Serial.println(input[indexFound-1]);
            }
            input.remove(indexFound-1, 1);
        }
        // look for anymore backspaces
        indexFound = input.indexOf(BACKSPACE);
    }

    if (verbose) {
        Serial.print("val: ");
        Serial.println(input);
        Serial.print("len: ");
        Serial.println(input.length());

        Serial.print("hex: ");
        for (size_t i = 0; i < input.length(); i++) {
            Serial.print(input[i], HEX);
            Serial.print(" ");
        }
        Serial.println();
    }

    return input;
}

void UserInput::ParseCommand(String input, String* command, std::vector<String>* parameters, char delimiter, size_t maxParameterCount)
{
    int splitIndex = input.indexOf(delimiter);
    if (splitIndex == -1) {
        // No parameters, input is command
        *command = input;
        command->trim();
        return;
    }
    *command = input.substring(0, splitIndex);
    command->trim();
    String allParams = input.substring(splitIndex + 1);

    while (allParams.length() > 0) {
        splitIndex = allParams.indexOf(delimiter);
        if (splitIndex == -1) {
            parameters->push_back(allParams);
            break;
        }

        parameters->push_back(allParams.substring(0, splitIndex));
        allParams = allParams.substring(splitIndex + 1);
    }
    // Serial.print("parameters: ");
    // Serial.println(parameters->size());
}