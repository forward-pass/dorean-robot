

#ifndef MOTOR_CONTROL_H
#define MOTOR_CONTROL_H

#include <Arduino.h>
#include "TMC5160.h"
#include <ArduinoJson.h>
#include "SPIFFS.h"
#include <SPI.h>

class MotorControl {
public:
    enum OperationMode {NormalMode, TestMode, CalibrationMode};

    enum CycleTargetFlag {NoneFlag, StartFlag, EndFlag, AdjustFlag};

    OperationMode ModeState = OperationMode::NormalMode;

    /// @brief Controller for sending commands to the TMC5160 motor controller through SPI.
    /// Base initialized as empty. To use, please provide the SPI cs/ss pin in the constructor.
    MotorControl();
    /// @brief Controller for sending commands to the TMC5160 motor controller through SPI.
    /// @attention Ensure that SPI.begin() has been run before running any function call that
    /// would communicate with the TMC. It is just recommended to run SPI begin before
    /// calling this constructor.
    /// @param SPI_CS The CS/SS pin for the TMC.
    MotorControl(int8_t SPI_CS);

    ~MotorControl();

    /// @brief Pings the motor controller for the existance of the TMC5160.
    /// This will confirm the connection to the motor controller and is
    /// infact the TMC5160. Different motor controllers have not been tested.
    /// @cite Tom Magnier for TMC5160_Arduino config wizard.
    void CheckMotorConnection();

    void InitializeMotor();

    /// @brief Calibrates the range of motion for the motor using stall detection as limits.
    void CalibrateRangeOfMotion(int adjust_limit_steps=10, int min_move_treshold=100, int update_threshold=500);

    /// @brief Moves the motor between start and end position in steps continuously.
    /// If calibrate has been successfully ran, then the motor will move between 
    /// 0% and 100% range of motion. This will ignore start and end values.
    /// When not calibrated, the start and end values will provide the step 
    /// position the motor will move between.
    /// @attention Requires ControlLoop() in the main loop() to operate.
    /// @param startStep Position in steps.
    /// @param endStep The end position in steps if provided. Otherwise, it is 
    /// NAN and it will move between 0 and start instead of between start and end.
    void TestCycle(float startStep=500, float endStep=NAN);

    void StopTest();

    void Move(float targetPos);

    void Step(float stepCount);

    bool ToggleVerbose();

    /// @brief 
    /// @param file 
    void SaveSettings(String file="motorSettings.json");

    void LoadSettings(String file);

    void GetCurrentSettings();
    void SetCurrentSettings();

    void PrintState();

    /// @brief Reset Stallguard stall condition.
    /// This is done by setting the SWMode sg_stop to 0 and back to 1.
    void ResetStall();

    void ControlLoop();

    float GetMaxSpeedRPM();
    float GetMaxSpeedSPS();
    void SetMaxSpeedRPM(float speedRPM);
    void SetMaxSpeedSPS(float speedSPS);

    TMC5160_SPI GetMotor();

private:
    // SPIClass spiComm;
    // SPIClass* spiComm = NULL;

    StaticJsonDocument<600> parameters;

    // TODO: Use enum instead
    bool MOTOR_TEST = false;
    bool VERBOSE = false;
    /// @brief Has calibrated its range of motion
    bool IS_CALIBRATED = false;

    float MaxSpeedSPS = 200;          // 1 full rotation = 200s/rev
    float MaxPosition = 3E+38;      // Used for homing position
    float VPWM = 80;
    float VCOOLTHRS = 150;
    float VHIHG = 400;
    /// @brief Delay between sending commands to the TMC motor. This is needed
    /// for the TMC to process the command properly when sending two commands sequentially.
    float ProcessingDelay = 500;

    // For printing data
    bool Verbose = false;

    // For motor testing purposes
    uint32_t dir_change_time    = 10000;
    uint32_t print_time         = 100;
    float   test_target         = 5000;

    // For Test mode motion cycle
    float cycleStartPos;
    float cycleEndPos;
    CycleTargetFlag cycleTarget = CycleTargetFlag::NoneFlag;

    // For calibration of range of motion
    CycleTargetFlag calibrationPhase = CycleTargetFlag::NoneFlag;
    int cal_adjust_limit_steps = 10;
    int cal_move_speed_sps = 400;
    int cal_min_move_treshold_steps = 100;
    int cal_update_threshold_steps = 500;
    int cal_start_pos = 0;
    int cal_temp_target = 0;

    TMC5160_SPI motor = TMC5160_SPI(5);
    // TMC5160_SPI motor;

    TMC5160::PowerStageParameters powerStageParams;
    TMC5160::MotorParameters motorParams;

    TMC5160_Reg::GCONF_Register gConf = { 0 };              // 0x00, Global configuration flags
    TMC5160_Reg::RAMP_STAT_Register rampStatus = { 0 };     // 0x35, Ramp status and switch event status
    TMC5160_Reg::IOIN_Register ioin = { 0 };                // 0x04, Read input / write output pins
    TMC5160_Reg::DRV_STATUS_Register drvStatus = { 0 };     // 0x6F, stallGuard2 value and driver error flags
    TMC5160_Reg::SW_MODE_Register swMode = { 0 };           // 0x34, Switch mode configuration
    TMC5160_Reg::IHOLD_IRUN_Register iHoldiRun = { 0 };     // 0x10, Driver current control
    TMC5160_Reg::COOLCONF_Register coolConf = { 0 };        // 0x6D, coolStep smart current control register and stallGuard2 configuration
    TMC5160_Reg::DRV_CONF_Register drvConf = { 0 };         // 0x0A, Driver configuration, used for pulse timings of the 4 motor windings.
    TMC5160_Reg::PWMCONF_Register pwmconf = { 0 };
    TMC5160_Reg::CHOPCONF_Register chopconf = { 0 };


    /// @brief Function which hold the control loop during Test mode.
    void operationTest();

    void operationCalibration();

    /// @brief Move the motor in a certain direction until the motor is stalled
    /// and return current position.
    /// @param motor Current motor object.
    /// @param drvStatus The register object, no point in making a new one everytime.
    /// @param initial_target Target position in number of full steps. Will move
    /// in the direction of the target position relative to the current position.
    /// @param move_speed Speed of the motor to find the stall.
    /// @param delay_time Wait a certain amount of time before motor commads to
    /// allow for the tmc to process.
    /// @param min_move_treshold Minimum movement until it starts checking stall.
    /// @param update_threshold Doubles the target position if current position
    /// is within this number of steps close to the previous target position.
    /// @return Returns the motor's final position after the stall.
    // float moveUntilStall(TMC5160_SPI motor, TMC5160_Reg::DRV_STATUS_Register drvStatus,
    //                     int initial_target, int move_speed = 400,
    //                     int min_move_treshold = 100, int update_threshold = 500);

    /// @brief Moves the motor between start and end position in steps continuously.
    /// @param startStep Position in steps.
    /// @param endStep The end position in steps if provided. Otherwise, it is 
    /// NAN and it will move between 0 and start instead of between start and end.
    void cycleMoveBlind(float startStep, float endStep=NAN);

    /// @brief Moves the motor between 0% and 100% range of motion. Must be
    /// calibrated to operate.
    void cycleMoveRangeOfMotion();

};


#endif // MOTOR_CONTROL_H