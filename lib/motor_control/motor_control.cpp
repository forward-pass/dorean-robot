


#include "motor_control.h"

MotorControl::MotorControl()
{
}

MotorControl::MotorControl(int8_t SPI_CS)
{
    motor = TMC5160_SPI(SPI_CS);
	digitalWrite(SPI_CS, HIGH);
}

MotorControl::~MotorControl()
{
    // delete spiComm;
}

void MotorControl::CheckMotorConnection()
{
    while (ioin.version != motor.IC_VERSION) {
        ioin.value = motor.readRegister(TMC5160_Reg::IO_INPUT_OUTPUT);

        if (ioin.value == 0 || ioin.value == 0xFFFFFFFF) {
            Serial.println("No TMC device found.");
            delay(2000);
        }
        else {
            Serial.println("Found a TMC device.");
            Serial.print("IC version: 0x");
            Serial.print(ioin.version, HEX);
            Serial.print(" (");
            if (ioin.version == motor.IC_VERSION) {
                Serial.println("TMC5160).");
            }
            else {
                Serial.println("WARNING: unknown IC! Expected TMC5160, code may not be compatible!");
            }
        }
    }
}

void MotorControl::InitializeMotor()
{
    swMode.value = motor.readRegister(TMC5160_Reg::SW_MODE);
    swMode.en_softstop = 0;
    swMode.sg_stop = 1;
    swMode.pol_stop_r = 0;
    swMode.pol_stop_l = 0;
    swMode.stop_r_enable = 0;
    swMode.stop_l_enable = 0;
    motor.writeRegister(TMC5160_Reg::SW_MODE, swMode.value);

    coolConf.value = motor.readRegister(TMC5160_Reg::COOLCONF);
    coolConf.sfilt = 0;
    coolConf.sgt = 3; // more sensitive [-64..63] less sensitive
    motor.writeRegister(TMC5160_Reg::COOLCONF, coolConf.value);

    powerStageParams.drvStrength = 2;
    powerStageParams.bbmTime = 0;
    powerStageParams.bbmClks = 4;
    motorParams.globalScaler = 98;
    motorParams.irun = 31;
    motorParams.ihold = 21;
    motorParams.freewheeling = TMC5160_Reg::FREEWHEEL_NORMAL;
    motorParams.pwmOfsInitial = 26;
    motorParams.pwmGradInitial = 35;
    motor.begin(powerStageParams, motorParams, TMC5160::NORMAL_MOTOR_DIRECTION);

    motor.setRampMode(TMC5160::POSITIONING_MODE);
    motor.setCurrentPosition(0);
    motor.setMaxSpeed(MaxSpeedSPS);
    motor.setRampSpeeds(0, 200, 200);
    // motor.setAccelerations(25, 70, 100, 100);
    motor.setAccelerations(500, 600, 1000, 1000); //motor.setAccelerations(25, 70, 100, 100);
    motor.setModeChangeSpeeds(VPWM, VCOOLTHRS, VHIHG);

    delay(1000); // Standstill for automatic tuning
}

void MotorControl::CalibrateRangeOfMotion(int adjust_limit_steps, int min_move_treshold, int update_threshold)
{
    cal_adjust_limit_steps = adjust_limit_steps;
    cal_min_move_treshold_steps = min_move_treshold;
    cal_update_threshold_steps = update_threshold;

    motor.setCurrentPosition(0);
    cal_start_pos = 0;
    calibrationPhase = CycleTargetFlag::StartFlag;
    cycleTarget = CycleTargetFlag::StartFlag;
    ModeState = OperationMode::CalibrationMode;
    cal_temp_target = -1000;

    motor.setTargetPosition(cal_temp_target);
    delay(ProcessingDelay);
}

void MotorControl::TestCycle(float startStep, float endStep)
{
    if (IS_CALIBRATED) {
        cycleMoveRangeOfMotion();
    }
    else {
        cycleMoveBlind(startStep, endStep);
    }
}

void MotorControl::StopTest()
{
    ModeState = OperationMode::NormalMode;
    cycleTarget = CycleTargetFlag::NoneFlag;
    motor.setTargetPosition(motor.getCurrentPosition());
}

void MotorControl::Move(float targetPos)
{
    // Only run after calibration
    if (!IS_CALIBRATED) {
        Serial.println("Homing Calibration has not completed.");
        Serial.println("Complete Calibration first or run the step command instead.");
        return;
    }

    float percent_pos = constrain(targetPos, 0, 100);
    float target = (percent_pos / 100.0f) * MaxPosition;
    // float prev_target = motor.getCurrentPosition();
    Serial.print("Stepping to: ");
    Serial.print(target);
    Serial.print(" (");
    Serial.print(percent_pos);
    Serial.println(")");
    Serial.print("From: ");
    Serial.print(motor.getCurrentPosition());
    Serial.print(" (");
    Serial.print((motor.getCurrentPosition() / MaxPosition) * 100.0f);
    Serial.println(")");
    motor.setTargetPosition(target);
}

void MotorControl::Step(float stepCount)
{
    Serial.print("Stepping from current position: ");
    Serial.print(stepCount);
    Serial.println(" steps");

    motor.setTargetPosition(motor.getCurrentPosition() + stepCount);
}

bool MotorControl::ToggleVerbose()
{
    Verbose = !Verbose;
    return Verbose;
}

void MotorControl::SaveSettings(String file)
{
    if (!file.startsWith("/")) {
        file = "/" + file;
    }

    if (!SPIFFS.begin(true)) {
        Serial.println("An Error has occurred while mounting SPIFFS");
        return;
    }

    File json_file = SPIFFS.open(file, FILE_WRITE);

    if (!json_file) {
        Serial.println("There was an error opening the file");
        return;
    }

    // GetCurrentSettings();

    // Write the parameters onto the json file.
    // serializeJson(parameters, json_file);
    size_t size = serializeJsonPretty(parameters, json_file);

    json_file.close();

    Serial.print(size);
    Serial.print(" bytes were written to ");
    Serial.println(file);
}

void MotorControl::LoadSettings(String file)
{
    if (!file.startsWith("/")) {
        file = "/" + file;
    }

    if (!SPIFFS.begin(true)) {
        Serial.println("An Error has occurred while mounting SPIFFS");
        return;
    }

    File json_file = SPIFFS.open(file, FILE_READ);

    if (!json_file) {
        Serial.println("There was an error opening the file");
        return;
    }

    String json = "";
    while (json_file.available()) {
        json.concat((char)json_file.read());
    }

    json_file.close();

    Serial.println(json);

    DeserializationError error = deserializeJson(parameters, json);

    if (error) {
        Serial.print(F("deserializeJson() failed: "));
        Serial.println(error.f_str());
        return;
    }

    // SetCurrentSettings();

    Serial.print("Settings were read successfully from ");
    Serial.println(file);
}

void MotorControl::GetCurrentSettings()
{
    gConf.value = motor.readRegister(TMC5160_Reg::GCONF);
    parameters["gConf__recalibrate"] =                                          (bool)gConf.recalibrate;
    parameters["gConf__faststandstill"] =                                       (bool)gConf.faststandstill;
    parameters["gConf__en_pwm_mode"] =                                          (bool)gConf.en_pwm_mode;
    parameters["gConf__multistep_filt"] =                                       (bool)gConf.multistep_filt;
    parameters["gConf__shaft"] =                                                (bool)gConf.shaft;
    parameters["gConf__diag0_error"] =                                          (bool)gConf.diag0_error;
    parameters["gConf__diag0_otpw"] =                                           (bool)gConf.diag0_otpw;
    parameters["gConf__diag0_stall_step"] =                                     (bool)gConf.diag0_stall_step;
    parameters["gConf__diag1_stall_dir"] =                                      (bool)gConf.diag1_stall_dir;
    parameters["gConf__diag1_index"] =                                          (bool)gConf.diag1_index;
    parameters["gConf__diag1_onstate"] =                                        (bool)gConf.diag1_onstate;
    parameters["gConf__diag1_steps_skipped"] =                                  (bool)gConf.diag1_steps_skipped;
    parameters["gConf__diag0_int_pushpull"] =                                   (bool)gConf.diag0_int_pushpull;
    parameters["gConf__diag1_poscomp_pushpull"] =                               (bool)gConf.diag1_poscomp_pushpull;
    parameters["gConf__small_hysteresis"] =                                     (bool)gConf.small_hysteresis;
    parameters["gConf__stop_enable"] =                                          (bool)gConf.stop_enable;
    parameters["gConf__direct_mode"] =                                          (bool)gConf.direct_mode;
    parameters["gConf__test_mode"] =                                            (bool)gConf.test_mode;

    rampStatus.value = motor.readRegister(TMC5160_Reg::RAMP_STAT);
    parameters["rampStatus__status_stop_l"] =                                   (bool)rampStatus.status_stop_l;
    parameters["rampStatus__status_stop_r"] =                                   (bool)rampStatus.status_stop_r;
    parameters["rampStatus__status_latch_l"] =                                  (bool)rampStatus.status_latch_l;
    parameters["rampStatus__status_latch_r"] =                                  (bool)rampStatus.status_latch_r;
    parameters["rampStatus__event_stop_l"] =                                    (bool)rampStatus.event_stop_l;
    parameters["rampStatus__event_stop_r"] =                                    (bool)rampStatus.event_stop_r;
    parameters["rampStatus__event_stop_sg"] =                                   (bool)rampStatus.event_stop_sg;
    parameters["rampStatus__event_pos_reached"] =                               (bool)rampStatus.event_pos_reached;
    parameters["rampStatus__velocity_reached"] =                                (bool)rampStatus.velocity_reached;
    parameters["rampStatus__position_reached"] =                                (bool)rampStatus.position_reached;
    parameters["rampStatus__vzero"] =                                           (bool)rampStatus.vzero;
    parameters["rampStatus__t_zerowait_active"] =                               (bool)rampStatus.t_zerowait_active;
    parameters["rampStatus__second_move"] =                                     (bool)rampStatus.second_move;
    parameters["rampStatus__status_sg"] =                                       (bool)rampStatus.status_sg;

    ioin.value = motor.readRegister(TMC5160_Reg::IO_INPUT_OUTPUT);
    parameters["ioin__refl_step"] =                                             (bool)ioin.refl_step;
    parameters["ioin__refr_dir"] =                                              (bool)ioin.refr_dir;
    parameters["ioin__encb_dcen_cfg4"] =                                        (bool)ioin.encb_dcen_cfg4;
    parameters["ioin__enca_dcin_cfg5"] =                                        (bool)ioin.enca_dcin_cfg5;
    parameters["ioin__drv_enn"] =                                               (bool)ioin.drv_enn;
    parameters["ioin__enc_n_dco_cfg6"] =                                        (bool)ioin.enc_n_dco_cfg6;
    parameters["ioin__sd_mode"] =                                               (bool)ioin.sd_mode;
    parameters["ioin__swcomp_in"] =                                             (bool)ioin.swcomp_in;
    parameters["ioin__version"] =                                               (uint8_t)ioin.version;

    drvStatus.value = motor.readRegister(TMC5160_Reg::DRV_STATUS);
    parameters["drvStatus__sg_result"] =                                        (bool)drvStatus.sg_result;
    parameters["drvStatus__s2vsa"] =                                            (bool)drvStatus.s2vsa;
    parameters["drvStatus__s2vsb"] =                                            (bool)drvStatus.s2vsb;
    parameters["drvStatus__stealth"] =                                          (bool)drvStatus.stealth;
    parameters["drvStatus__fsactive"] =                                         (bool)drvStatus.fsactive;
    parameters["drvStatus__cs_actual"] =                                        (bool)drvStatus.cs_actual;
    parameters["drvStatus__stallguard"] =                                       (bool)drvStatus.stallguard;
    parameters["drvStatus__ot"] =                                               (bool)drvStatus.ot;
    parameters["drvStatus__otpw"] =                                             (bool)drvStatus.otpw;
    parameters["drvStatus__s2ga"] =                                             (bool)drvStatus.s2ga;
    parameters["drvStatus__s2gb"] =                                             (bool)drvStatus.s2gb;
    parameters["drvStatus__ola"] =                                              (bool)drvStatus.ola;
    parameters["drvStatus__olb"] =                                              (bool)drvStatus.olb;
    parameters["drvStatus__stst"] =                                             (bool)drvStatus.stst;

    swMode.value = motor.readRegister(TMC5160_Reg::SW_MODE);
    parameters["swMode__stop_l_enable"] =                                       (bool)swMode.stop_l_enable;
    parameters["swMode__stop_r_enable"] =                                       (bool)swMode.stop_r_enable;
    parameters["swMode__pol_stop_l"] =                                          (bool)swMode.pol_stop_l;
    parameters["swMode__pol_stop_r"] =                                          (bool)swMode.pol_stop_r;
    parameters["swMode__swap_lr"] =                                             (bool)swMode.swap_lr;
    parameters["swMode__latch_l_active"] =                                      (bool)swMode.latch_l_active;
    parameters["swMode__latch_l_inactive"] =                                    (bool)swMode.latch_l_inactive;
    parameters["swMode__latch_r_active"] =                                      (bool)swMode.latch_r_active;
    parameters["swMode__latch_r_inactive"] =                                    (bool)swMode.latch_r_inactive;
    parameters["swMode__en_latch_encoder"] =                                    (bool)swMode.en_latch_encoder;
    parameters["swMode__sg_stop"] =                                             (bool)swMode.sg_stop;
    parameters["swMode__en_softstop"] =                                         (bool)swMode.en_softstop;

    iHoldiRun.value = motor.readRegister(TMC5160_Reg::IHOLD_IRUN);
    parameters["iHoldiRun__ihold"] =                                            (uint8_t)iHoldiRun.ihold;
    parameters["iHoldiRun__irun"] =                                             (uint8_t)iHoldiRun.irun;
    parameters["iHoldiRun__iholddelay"] =                                       (uint8_t)iHoldiRun.iholddelay;

    coolConf.value = motor.readRegister(TMC5160_Reg::COOLCONF);
    parameters["coolConf__semin"] =                                             (uint8_t)coolConf.semin;
    parameters["coolConf__seup"] =                                              (uint8_t)coolConf.seup;
    parameters["coolConf__semax"] =                                             (uint8_t)coolConf.semax;
    parameters["coolConf__sedn"] =                                              (uint8_t)coolConf.sedn;
    parameters["coolConf__seimin"] =                                            (bool)coolConf.seimin;
    parameters["coolConf__sgt"] =                                               (uint8_t)coolConf.sgt;
    parameters["coolConf__sfilt"] =                                             (bool)coolConf.sfilt;

    drvConf.value = motor.readRegister(TMC5160_Reg::DRV_CONF);
    parameters["drvConf__bbmtime"] =                                            (uint8_t)drvConf.bbmtime;
    parameters["drvConf__bbmclks"] =                                            (uint8_t)drvConf.bbmclks;
    parameters["drvConf__otselect"] =                                           (uint8_t)drvConf.otselect;
    parameters["drvConf__drvstrength"] =                                        (uint8_t)drvConf.drvstrength;
    parameters["drvConf__filt_isense"] =                                        (uint8_t)drvConf.filt_isense;

    pwmconf.value = motor.readRegister(TMC5160_Reg::PWMCONF);
    parameters["pwmconf__pwm_ofs"] =                                            (uint8_t)pwmconf.pwm_ofs;
    parameters["pwmconf__pwm_grad"] =                                           (uint8_t)pwmconf.pwm_grad;
    parameters["pwmconf__pwm_freq"] =                                           (uint8_t)pwmconf.pwm_freq;
    parameters["pwmconf__pwm_autoscale"] =                                      (bool)pwmconf.pwm_autoscale;
    parameters["pwmconf__pwm_autograd"] =                                       (bool)pwmconf.pwm_autograd;
    parameters["pwmconf__freewheel"] =                                          (uint8_t)pwmconf.freewheel;
    parameters["pwmconf__pwm_reg"] =                                            (uint8_t)pwmconf.pwm_reg;
    parameters["pwmconf__pwm_lim"] =                                            (uint8_t)pwmconf.pwm_lim;

    chopconf.value = motor.readRegister(TMC5160_Reg::CHOPCONF);
    parameters["chopconf__toff"] =                                              (uint8_t)chopconf.toff;
    parameters["chopconf__hstrt_tfd"] =                                         (uint8_t)chopconf.hstrt_tfd;
    parameters["chopconf__hend_offset"] =                                       (uint8_t)chopconf.hend_offset;
    parameters["chopconf__tfd_3"] =                                             (bool)chopconf.tfd_3;
    parameters["chopconf__disfdcc"] =                                           (bool)chopconf.disfdcc;
    parameters["chopconf__rndtf"] =                                             (bool)chopconf.rndtf;
    parameters["chopconf__chm"] =                                               (bool)chopconf.chm;
    parameters["chopconf__tbl"] =                                               (uint8_t)chopconf.tbl;
    parameters["chopconf__vsense"] =                                            (bool)chopconf.vsense;
    parameters["chopconf__vhighfs"] =                                           (bool)chopconf.vhighfs;
    parameters["chopconf__vhighchm"] =                                          (bool)chopconf.vhighchm;
    parameters["chopconf__tpfd"] =                                              (uint8_t)chopconf.tpfd;
    parameters["chopconf__mres"] =                                              (uint8_t)chopconf.mres;
    parameters["chopconf__intpol"] =                                            (bool)chopconf.intpol;
    parameters["chopconf__dedge"] =                                             (bool)chopconf.dedge;
    parameters["chopconf__diss2g"] =                                            (bool)chopconf.diss2g;
    parameters["chopconf__diss2vs"] =                                           (bool)chopconf.diss2vs;

    parameters["globalScaler"] = motor.readRegister(TMC5160_Reg::GLOBAL_SCALER);
}

void MotorControl::SetCurrentSettings()
{
    gConf.recalibrate =                             parameters["gConf__recalibrate"].as<bool>();
    gConf.faststandstill =                          parameters["gConf__faststandstill"].as<bool>();
    gConf.en_pwm_mode =                             parameters["gConf__en_pwm_mode"].as<bool>();
    gConf.multistep_filt =                          parameters["gConf__multistep_filt"].as<bool>();
    gConf.shaft =                                   parameters["gConf__shaft"].as<bool>();
    gConf.diag0_error =                             parameters["gConf__diag0_error"].as<bool>();
    gConf.diag0_otpw =                              parameters["gConf__diag0_otpw"].as<bool>();
    gConf.diag0_stall_step =                        parameters["gConf__diag0_stall_step"].as<bool>();
    gConf.diag1_stall_dir =                         parameters["gConf__diag1_stall_dir"].as<bool>();
    gConf.diag1_index =                             parameters["gConf__diag1_index"].as<bool>();
    gConf.diag1_onstate =                           parameters["gConf__diag1_onstate"].as<bool>();
    gConf.diag1_steps_skipped =                     parameters["gConf__diag1_steps_skipped"].as<bool>();
    gConf.diag0_int_pushpull =                      parameters["gConf__diag0_int_pushpull"].as<bool>();
    gConf.diag1_poscomp_pushpull =                  parameters["gConf__diag1_poscomp_pushpull"].as<bool>();
    gConf.small_hysteresis =                        parameters["gConf__small_hysteresis"].as<bool>();
    gConf.stop_enable =                             parameters["gConf__stop_enable"].as<bool>();
    gConf.direct_mode =                             parameters["gConf__direct_mode"].as<bool>();
    gConf.test_mode =                               parameters["gConf__test_mode"].as<bool>();
    motor.writeRegister(TMC5160_Reg::GCONF, gConf.value);

    rampStatus.status_stop_l =                      parameters["rampStatus__status_stop_l"].as<bool>();
    rampStatus.status_stop_r =                      parameters["rampStatus__status_stop_r"].as<bool>();
    rampStatus.status_latch_l =                     parameters["rampStatus__status_latch_l"].as<bool>();
    rampStatus.status_latch_r =                     parameters["rampStatus__status_latch_r"].as<bool>();
    rampStatus.event_stop_l =                       parameters["rampStatus__event_stop_l"].as<bool>();
    rampStatus.event_stop_r =                       parameters["rampStatus__event_stop_r"].as<bool>();
    rampStatus.event_stop_sg =                      parameters["rampStatus__event_stop_sg"].as<bool>();
    rampStatus.event_pos_reached =                  parameters["rampStatus__event_pos_reached"].as<bool>();
    rampStatus.velocity_reached =                   parameters["rampStatus__velocity_reached"].as<bool>();
    rampStatus.position_reached =                   parameters["rampStatus__position_reached"].as<bool>();
    rampStatus.vzero =                              parameters["rampStatus__vzero"].as<bool>();
    rampStatus.t_zerowait_active =                  parameters["rampStatus__t_zerowait_active"].as<bool>();
    rampStatus.second_move =                        parameters["rampStatus__second_move"].as<bool>();
    rampStatus.status_sg =                          parameters["rampStatus__status_sg"].as<bool>();
    motor.writeRegister(TMC5160_Reg::RAMP_STAT, rampStatus.value);

    ioin.refl_step =                                parameters["ioin__refl_step"].as<bool>();
    ioin.refr_dir =                                 parameters["ioin__refr_dir"].as<bool>();
    ioin.encb_dcen_cfg4 =                           parameters["ioin__encb_dcen_cfg4"].as<bool>();
    ioin.enca_dcin_cfg5 =                           parameters["ioin__enca_dcin_cfg5"].as<bool>();
    ioin.drv_enn =                                  parameters["ioin__drv_enn"].as<bool>();
    ioin.enc_n_dco_cfg6 =                           parameters["ioin__enc_n_dco_cfg6"].as<bool>();
    ioin.sd_mode =                                  parameters["ioin__sd_mode"].as<bool>();
    ioin.swcomp_in =                                parameters["ioin__swcomp_in"].as<bool>();
    ioin.version =                                  parameters["ioin__version"].as<uint8_t>();
    motor.writeRegister(TMC5160_Reg::IO_INPUT_OUTPUT, ioin.value);

    drvStatus.sg_result =                           parameters["drvStatus__sg_result"].as<bool>();
    drvStatus.s2vsa =                               parameters["drvStatus__s2vsa"].as<bool>();
    drvStatus.s2vsb =                               parameters["drvStatus__s2vsb"].as<bool>();
    drvStatus.stealth =                             parameters["drvStatus__stealth"].as<bool>();
    drvStatus.fsactive =                            parameters["drvStatus__fsactive"].as<bool>();
    drvStatus.cs_actual =                           parameters["drvStatus__cs_actual"].as<bool>();
    drvStatus.stallguard =                          parameters["drvStatus__stallguard"].as<bool>();
    drvStatus.ot =                                  parameters["drvStatus__ot"].as<bool>();
    drvStatus.otpw =                                parameters["drvStatus__otpw"].as<bool>();
    drvStatus.s2ga =                                parameters["drvStatus__s2ga"].as<bool>();
    drvStatus.s2gb =                                parameters["drvStatus__s2gb"].as<bool>();
    drvStatus.ola =                                 parameters["drvStatus__ola"].as<bool>();
    drvStatus.olb =                                 parameters["drvStatus__olb"].as<bool>();
    drvStatus.stst =                                parameters["drvStatus__stst"].as<bool>();
    motor.writeRegister(TMC5160_Reg::DRV_STATUS, drvStatus.value);

    swMode.stop_l_enable =                          parameters["swMode__stop_l_enable"].as<bool>();
    swMode.stop_r_enable =                          parameters["swMode__stop_r_enable"].as<bool>();
    swMode.pol_stop_l =                             parameters["swMode__pol_stop_l"].as<bool>();
    swMode.pol_stop_r =                             parameters["swMode__pol_stop_r"].as<bool>();
    swMode.swap_lr =                                parameters["swMode__swap_lr"].as<bool>();
    swMode.latch_l_active =                         parameters["swMode__latch_l_active"].as<bool>();
    swMode.latch_l_inactive =                       parameters["swMode__latch_l_inactive"].as<bool>();
    swMode.latch_r_active =                         parameters["swMode__latch_r_active"].as<bool>();
    swMode.latch_r_inactive =                       parameters["swMode__latch_r_inactive"].as<bool>();
    swMode.en_latch_encoder =                       parameters["swMode__en_latch_encoder"].as<bool>();
    swMode.sg_stop =                                parameters["swMode__sg_stop"].as<bool>();
    swMode.en_softstop =                            parameters["swMode__en_softstop"].as<bool>();
    motor.writeRegister(TMC5160_Reg::SW_MODE, swMode.value);

    iHoldiRun.ihold =                               parameters["iHoldiRun__ihold"].as<uint8_t>();
    iHoldiRun.irun =                                parameters["iHoldiRun__irun"].as<uint8_t>();
    iHoldiRun.iholddelay =                          parameters["iHoldiRun__iholddelay"].as<uint8_t>();
    motor.writeRegister(TMC5160_Reg::IHOLD_IRUN, iHoldiRun.value);

    coolConf.semin =                                parameters["coolConf__semin"].as<uint8_t>();
    coolConf.seup =                                 parameters["coolConf__seup"].as<uint8_t>();
    coolConf.semax =                                parameters["coolConf__semax"].as<uint8_t>();
    coolConf.sedn =                                 parameters["coolConf__sedn"].as<uint8_t>();
    coolConf.seimin =                               parameters["coolConf__seimin"].as<bool>();
    coolConf.sgt =                                  parameters["coolConf__sgt"].as<uint8_t>();
    coolConf.sfilt =                                parameters["coolConf__sfilt"].as<bool>();
    motor.writeRegister(TMC5160_Reg::COOLCONF, coolConf.value);

    drvConf.bbmtime =                               parameters["drvConf__bbmtime"].as<uint8_t>();
    drvConf.bbmclks =                               parameters["drvConf__bbmclks"].as<uint8_t>();
    drvConf.otselect =                              parameters["drvConf__otselect"].as<uint8_t>();
    drvConf.drvstrength =                           parameters["drvConf__drvstrength"].as<uint8_t>();
    drvConf.filt_isense =                           parameters["drvConf__filt_isense"].as<uint8_t>();
    motor.writeRegister(TMC5160_Reg::DRV_CONF, drvConf.value);

    pwmconf.pwm_ofs =                               parameters["pwmconf__pwm_ofs"].as<uint8_t>();
    pwmconf.pwm_grad =                              parameters["pwmconf__pwm_grad"].as<uint8_t>();
    pwmconf.pwm_freq =                              parameters["pwmconf__pwm_freq"].as<uint8_t>();
    pwmconf.pwm_autoscale =                         parameters["pwmconf__pwm_autoscale"].as<bool>();
    pwmconf.pwm_autograd =                          parameters["pwmconf__pwm_autograd"].as<bool>();
    pwmconf.freewheel =                             parameters["pwmconf__freewheel"].as<uint8_t>();
    pwmconf.pwm_reg =                               parameters["pwmconf__pwm_reg"].as<uint8_t>();
    pwmconf.pwm_lim =                               parameters["pwmconf__pwm_lim"].as<uint8_t>();
    motor.writeRegister(TMC5160_Reg::PWMCONF, pwmconf.value);

    chopconf.toff =                                 parameters["chopconf__toff"].as<uint8_t>();
    chopconf.hstrt_tfd =                            parameters["chopconf__hstrt_tfd"].as<uint8_t>();
    chopconf.hend_offset =                          parameters["chopconf__hend_offset"].as<uint8_t>();
    chopconf.tfd_3 =                                parameters["chopconf__tfd_3"].as<bool>();
    chopconf.disfdcc =                              parameters["chopconf__disfdcc"].as<bool>();
    chopconf.rndtf =                                parameters["chopconf__rndtf"].as<bool>();
    chopconf.chm =                                  parameters["chopconf__chm"].as<bool>();
    chopconf.tbl =                                  parameters["chopconf__tbl"].as<uint8_t>();
    chopconf.vsense =                               parameters["chopconf__vsense"].as<bool>();
    chopconf.vhighfs =                              parameters["chopconf__vhighfs"].as<bool>();
    chopconf.vhighchm =                             parameters["chopconf__vhighchm"].as<bool>();
    chopconf.tpfd =                                 parameters["chopconf__tpfd"].as<uint8_t>();
    chopconf.mres =                                 parameters["chopconf__mres"].as<uint8_t>();
    chopconf.intpol =                               parameters["chopconf__intpol"].as<bool>();
    chopconf.dedge =                                parameters["chopconf__dedge"].as<bool>();
    chopconf.diss2g =                               parameters["chopconf__diss2g"].as<bool>();
    chopconf.diss2vs =                              parameters["chopconf__diss2vs"].as<bool>();
    motor.writeRegister(TMC5160_Reg::CHOPCONF, chopconf.value);

    motor.writeRegister(TMC5160_Reg::GLOBAL_SCALER, parameters["globalScaler"].as<uint8_t>());
}

void MotorControl::PrintState()
{
    // get the current target position
    float xactual = motor.getCurrentPosition();
    float vactual = motor.getCurrentSpeed();
    Serial.print("X:"); Serial.print(xactual/200.0f * 36.0f); Serial.print(", ");
    Serial.print("V_rpm:"); Serial.print(vactual/200.0f * 60.0f); Serial.print(", ");
    Serial.print("V_sps:"); Serial.print(vactual); Serial.print(", ");

    drvStatus.value = motor.readRegister(TMC5160_Reg::DRV_STATUS);
    Serial.print("Stall_Value:"); Serial.print(drvStatus.sg_result, DEC); Serial.print(", ");
    Serial.print("Stall:"); Serial.print(drvStatus.stallguard ? true:false); Serial.print(", ");
    Serial.print("Current:"); Serial.print(drvStatus.cs_actual, DEC); Serial.print(", ");
    uint32_t tstep = motor.readRegister(TMC5160_Reg::TSTEP);
    float tstep_speed = 12000000.0/(256.0*tstep);
    Serial.print("tstep_speed:"); Serial.print(tstep_speed); Serial.print(", ");
    // Serial.print("TSTEP:"); Serial.print(motor.readRegister(TMC5160_Reg::TSTEP), DEC); Serial.print(", ");
    Serial.print("TPWMTHRS:"); Serial.print(motor.readRegister(TMC5160_Reg::TPWMTHRS), DEC); Serial.print(", ");
    Serial.print("TCOOLTHRS:"); Serial.print(motor.readRegister(TMC5160_Reg::TCOOLTHRS), DEC); Serial.print(", ");
    Serial.print("THIGH:"); Serial.print(motor.readRegister(TMC5160_Reg::THIGH), DEC); Serial.print(", ");

    rampStatus.value = motor.readRegister(TMC5160_Reg::RAMP_STAT);
    Serial.print("position_reached:"); Serial.print(rampStatus.position_reached ? true : false); Serial.print(", ");
    Serial.println();
}

void MotorControl::ResetStall()
{
    swMode.sg_stop = 0;
    motor.writeRegister(TMC5160_Reg::SW_MODE, swMode.value);
    swMode.sg_stop = 1;
    motor.writeRegister(TMC5160_Reg::SW_MODE, swMode.value);
}

void MotorControl::ControlLoop()
{
    // Runs loop in arduino loop function
    // Everything needs to be non-blocking!
    switch (ModeState)
    {
    case OperationMode::NormalMode:
        /* code */
        break;
    case OperationMode::CalibrationMode:
        operationCalibration();
        break;
    case OperationMode::TestMode:
        operationTest();
        break;

    default:
        break;
    }

    if (Verbose) {
        PrintState();
    }
}

float MotorControl::GetMaxSpeedRPM()
{
    return (MaxSpeedSPS * 60.0f) / 200.0f;
}

float MotorControl::GetMaxSpeedSPS()
{
    return MaxSpeedSPS;
}

void MotorControl::SetMaxSpeedRPM(float speedRPM)
{
    // Max rpm is 400 => 1333 steps per second
    float speedStepsPerSecond = (speedRPM * 200.0f) / 60.0f;
    MaxSpeedSPS = constrain(speedStepsPerSecond, 0.0f, 1333.33f);
    motor.setMaxSpeed(MaxSpeedSPS);
}

void MotorControl::SetMaxSpeedSPS(float speedSPS)
{
    // Max rpm is 400 => 1333 steps per second
    MaxSpeedSPS = constrain(speedSPS, 0.0f, 1333.33f);
    motor.setMaxSpeed(MaxSpeedSPS);
}

TMC5160_SPI MotorControl::GetMotor()
{
    return motor;
}

void MotorControl::operationTest()
{
    switch (cycleTarget)
    {
    // If target start and reached start
        // Change target to end
    case CycleTargetFlag::StartFlag:
        if (motor.isTargetPositionReached()) {
            cycleTarget = CycleTargetFlag::EndFlag;
            motor.setTargetPosition(cycleEndPos);
        }
        break;
    // If target end and reached end
        // Change target to start
    case CycleTargetFlag::EndFlag:
        if (motor.isTargetPositionReached()) {
            cycleTarget = CycleTargetFlag::StartFlag;
            motor.setTargetPosition(cycleStartPos);
        }
        break;

    // If target is None
        // Go back to normal mode
    case CycleTargetFlag::NoneFlag:
    default:
        Serial.println("Test mode cycleTarget None invalid. Returning to normal mode.");
        ModeState = OperationMode::NormalMode;
        break;
    }
}

void MotorControl::operationCalibration()
{
    switch (cycleTarget)
    {
    case CycleTargetFlag::StartFlag:
        drvStatus.value = motor.readRegister(TMC5160_Reg::DRV_STATUS);
        // if target start, stallguard has activated, and min distance have moved 
        if ( (drvStatus.stallguard ? true : false) &&
            abs(motor.getCurrentPosition() - cal_start_pos) >= cal_min_move_treshold_steps) {
            // Stop the motor and reset stall
            Serial.println("Reached min pos limit.");
            motor.setTargetPosition(motor.getCurrentPosition());
            delay(ProcessingDelay);
            ResetStall();
            Serial.println("Adjusting pos for stall prevention.");
            // Move back a little bit with the adjust flag
            int adjust_target = motor.getCurrentPosition() + cal_adjust_limit_steps;
            cycleTarget = CycleTargetFlag::AdjustFlag;
            motor.setTargetPosition(adjust_target);
        }
        // else if temp target is almost reached, double the distance.
        else if (motor.getCurrentPosition() - cal_temp_target < cal_update_threshold_steps) {
            cal_temp_target *= 2;
            motor.setTargetPosition(cal_temp_target);
        }
        break;

    case CycleTargetFlag::EndFlag:
        drvStatus.value = motor.readRegister(TMC5160_Reg::DRV_STATUS);
        // if target end, stallguard has activated, and min distance have moved 
        if ( (drvStatus.stallguard ? true : false) &&
            abs(motor.getCurrentPosition() - cal_start_pos) >= cal_min_move_treshold_steps) {
            // Stop the motor and reset stall
            Serial.println("Reached max pos limit.");
            motor.setTargetPosition(motor.getCurrentPosition());
            delay(ProcessingDelay);
            ResetStall();
            Serial.println("Adjusting pos for stall prevention.");
            // Move back a little bit with the adjust flag
            int adjust_target = motor.getCurrentPosition() - cal_adjust_limit_steps;
            cycleTarget = CycleTargetFlag::AdjustFlag;
            motor.setTargetPosition(adjust_target);
        }
        // else if temp target is almost reached, double the distance.
        else if (motor.getCurrentPosition() - cal_temp_target < cal_update_threshold_steps) {
            cal_temp_target *= 2;
            motor.setTargetPosition(cal_temp_target);
        }
        break;

    case CycleTargetFlag::AdjustFlag:
        // If motor has reached adjustment, set position as range of motion limits
        if (!motor.isTargetPositionReached()) {
            break;
        }

        Serial.println("Reached adjusted pos.");
        if (calibrationPhase == CycleTargetFlag::StartFlag) {
            motor.setCurrentPosition(0);
            cal_start_pos = 0;
            cal_temp_target = 1000;
            calibrationPhase = CycleTargetFlag::EndFlag;
            cycleTarget = CycleTargetFlag::EndFlag;

            Serial.println("Start targeting positive pos limit.");
            motor.setTargetPosition(cal_temp_target);
            delay(ProcessingDelay);
        }
        else if (calibrationPhase == CycleTargetFlag::EndFlag) {
            MaxPosition = motor.getCurrentPosition();
            IS_CALIBRATED = true;

            Serial.println("Moving back to middle pos.");
            motor.setMaxSpeed(MaxSpeedSPS);
            motor.setTargetPosition(0.5*MaxPosition);

            cycleTarget = CycleTargetFlag::NoneFlag;
            calibrationPhase = CycleTargetFlag::NoneFlag;
            ModeState = OperationMode::NormalMode;
        }
        break;

    case CycleTargetFlag::NoneFlag:
    default:
        Serial.println("Calibration mode cycleTarget None invalid. Returning to normal mode.");
        ModeState = OperationMode::NormalMode;
        break;
    }
}

void MotorControl::cycleMoveBlind(float startStep, float endStep)
{
    cycleStartPos = startStep;
    cycleEndPos = endStep;
    if (isnan(endStep)) {
        cycleStartPos = 0;
        cycleEndPos = startStep;
    }

    ModeState = OperationMode::TestMode;
    cycleTarget = CycleTargetFlag::StartFlag;
    motor.setTargetPosition(cycleStartPos);
}

void MotorControl::cycleMoveRangeOfMotion()
{
    cycleStartPos = 0;
    cycleEndPos = MaxPosition;

    ModeState = OperationMode::TestMode;
    cycleTarget = CycleTargetFlag::StartFlag;
    motor.setTargetPosition(cycleStartPos);
}