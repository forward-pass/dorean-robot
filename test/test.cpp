#include <Arduino.h>
#include <unity.h>
#include "motor_control.h"
#include "user_input.h"

void setUp(void) {
    // set stuff up here
}

void tearDown(void) {
    // clean stuff up here
}

void testReadWriteFile() {
    String testFileName = "/testFile.txt";
    if (!testFileName.startsWith("/")) {
        testFileName = "/" + testFileName;
    }
    TEST_ASSERT_TRUE(SPIFFS.begin(true));

    SPIFFS.remove(testFileName);

    TEST_ASSERT_FALSE(SPIFFS.exists(testFileName));

    File testFileWrite = SPIFFS.open(testFileName, FILE_WRITE);

    TEST_ASSERT_TRUE(testFileWrite);
    TEST_ASSERT_TRUE(SPIFFS.exists(testFileName));

    String testString = "test 123.\n Something else!";

    TEST_ASSERT_TRUE(testFileWrite.print(testString));

    testFileWrite.close();

    File testFileRead = SPIFFS.open(testFileName, FILE_READ);

    TEST_ASSERT_TRUE(testFileRead);

    String readBack = "";
    while (testFileRead.available()) {
        readBack.concat((char)testFileRead.read());
        // readBack.concat("\n");
    }

    testFileRead.close();

    // Serial.println(testString);
    // Serial.println(readBack);
    TEST_ASSERT_TRUE(testString == readBack);

    SPIFFS.remove(testFileName);

    TEST_ASSERT_FALSE(SPIFFS.exists(testFileName));
}

// TODO: test SaveSettings and LoadSettings with dummy parameters

// TODO: test SaveSettings and LoadSettings with all default parameters



void testStringCompare() {
    String sample1 = "Hello World!";
    String sample2 = "Hello World!" + String(UserInput::NEWLINE);
    String sample3 = "Hello World!" + String(UserInput::CARAGE_RETURN);
    String sample4 = "Hello World?" + String(UserInput::BACKSPACE) + "!";

    TEST_ASSERT_FALSE(sample1 == sample2);
    TEST_ASSERT_FALSE(sample1 == sample3);
    TEST_ASSERT_FALSE(sample1 == sample4);

    TEST_ASSERT_FALSE(sample2 == sample3);
    TEST_ASSERT_FALSE(sample2 == sample4);

    TEST_ASSERT_FALSE(sample3 == sample4);
}

void testGetInputNormal() {
    String sample1 = "Hello World!";
    String processed_sample1 = UserInput::GetInput(false, sample1);

    TEST_ASSERT_EQUAL_STRING(sample1.c_str(), processed_sample1.c_str());
}

void testGetInputEmpty() {
    String sample1 = "";
    String processed_sample1 = UserInput::GetInput(false, sample1);

    String sample2 = "\n";
    String processed_sample2 = UserInput::GetInput(false, sample2);

    String sample3 = "\r";
    String processed_sample3 = UserInput::GetInput(false, sample3);

    String sample4 = " ";
    String processed_sample4 = UserInput::GetInput(true, sample4);

    TEST_ASSERT_EQUAL_STRING(sample1.c_str(), processed_sample1.c_str());
    TEST_ASSERT_EQUAL_STRING(sample1.c_str(), processed_sample2.c_str());
    TEST_ASSERT_EQUAL_STRING(sample1.c_str(), processed_sample3.c_str());
    TEST_ASSERT_EQUAL_STRING(sample1.c_str(), processed_sample4.c_str());
}

void testGetInputMultipleUnnecessaryEndings() {
    String sample1 = "Something!";
    String processed_sample1 = UserInput::GetInput(false, sample1);

    String sample2 = "Something!\n\n\n" + String(UserInput::NEWLINE);
    String processed_sample2 = UserInput::GetInput(false, sample2);

    String sample3 = "Something!\r \n \r \n " + String(UserInput::CARAGE_RETURN);
    String processed_sample3 = UserInput::GetInput(false, sample3);

    String sample4 = "Something!              ";
    String processed_sample4 = UserInput::GetInput(false, sample4);

    String sample5 = "Something! " + String(UserInput::SPACE);
    String processed_sample5 = UserInput::GetInput(false, sample5);

    String sample6 = "\nSomething!";
    String processed_sample6 = UserInput::GetInput(false, sample6);

    String sample7 = " Something!";
    String processed_sample7 = UserInput::GetInput(false, sample7);

    TEST_ASSERT_EQUAL_STRING(sample1.c_str(), processed_sample1.c_str());
    TEST_ASSERT_EQUAL_STRING(sample1.c_str(), processed_sample2.c_str());
    TEST_ASSERT_EQUAL_STRING(sample1.c_str(), processed_sample3.c_str());
    TEST_ASSERT_EQUAL_STRING(sample1.c_str(), processed_sample4.c_str());
    TEST_ASSERT_EQUAL_STRING(sample1.c_str(), processed_sample5.c_str());

    TEST_ASSERT_FALSE(sample1 == processed_sample6);
    TEST_ASSERT_FALSE(sample1 == processed_sample7);
}

void testGetInputBackspaces() {
    String sample1 = "Something!";
    String processed_sample1 = UserInput::GetInput(false, sample1);

    String sample2 = "Somewhat?" + 
        String(UserInput::BACKSPACE) + String(UserInput::BACKSPACE) + 
        String(UserInput::BACKSPACE) + String(UserInput::BACKSPACE) + 
        String(UserInput::BACKSPACE) + "thing!";
    String processed_sample2 = UserInput::GetInput(false, sample2);

    String sample3 = String(UserInput::BACKSPACE) + "Something!";
    String processed_sample3 = UserInput::GetInput(false, sample3);

    String sample4 = String(UserInput::BACKSPACE) + "Something#" + 
        String(UserInput::BACKSPACE) + "!";
    String processed_sample4 = UserInput::GetInput(false, sample4);

    TEST_ASSERT_EQUAL_STRING(sample1.c_str(), processed_sample1.c_str());
    TEST_ASSERT_EQUAL_STRING(sample1.c_str(), processed_sample2.c_str());
    TEST_ASSERT_EQUAL_STRING(sample1.c_str(), processed_sample3.c_str());
    TEST_ASSERT_EQUAL_STRING(sample1.c_str(), processed_sample4.c_str());
}

void setup()
{
    // delay(2000); // service delay
    Serial.begin(115200);

    UNITY_BEGIN();

    RUN_TEST(testReadWriteFile);

    RUN_TEST(testStringCompare);
    RUN_TEST(testGetInputNormal);
    RUN_TEST(testGetInputEmpty);
    RUN_TEST(testGetInputMultipleUnnecessaryEndings);
    RUN_TEST(testGetInputBackspaces);

    UNITY_END(); // stop unit testing
}

void loop()
{

}